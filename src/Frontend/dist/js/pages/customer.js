import * as adminLteHelper from "../common/adminlte-utils.js";
import * as modalHelper from "../common/modal-utils.js";
import * as dataTableHelper from "../common/datatable-utils.js";
import * as errorHandler from "../common/errorHandler.js";
import * as controlHelper from "../common/controls-utils.js";
import * as dateTimeHelper from "../common/datetime-ultis.js";

"use strict";
const ACTIVE_MENU_CONTENT = "Customers";
const BASE_URL = "http://localhost:8080";
var gDATA_TABLE;
var gSTUDENT_ENTITY

$(document).ready(function () {
    adminLteHelper.setActiveMenu(ACTIVE_MENU_CONTENT);
    adminLteHelper.setActiveBreadcrumbItemContent(ACTIVE_MENU_CONTENT);

    gSTUDENT_ENTITY = new StudentEntity();

    addEventListener();
    initDatatable();
    //loadAllStudents();
    //loadClassroomToSelectClassroom();
});

function initDatatable() {
    gDATA_TABLE = $("#student-table").DataTable({
        "columns": [
            {
                render: function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },
            { data: "studentCode", defaultContent: "" },//set default content to avoid warning when data is null
            { data: "studentName", defaultContent: "" },
            { data: "gender", defaultContent: "" },
            {
                data: "birthday",
                defaultContent: "",
                render: function (data, type, row, meta) {
                    return dateTimeHelper.formatDate(row.birthday, "dd/MM/yyyy");
                }
            },
            { data: "address", defaultContent: "" },
            { data: "phoneNumber", defaultContent: "" },
            { data: "classroomName", defaultContent: "" },
            {
                data: "createdDate",
                defaultContent: "",
                render: function (data, type, row, meta) {
                    return dateTimeHelper.formatDate(row.createdDate, "dd/MM/yyyy");
                }
            },
            {
                data: "updatedDate",
                defaultContent: "",
                render: function (data, type, row, meta) {
                    return dateTimeHelper.formatDate(row.updatedDate, "dd/MM/yyyy");
                }
            },
            {
                defaultContent:
                    `
                <i class="fa fa-edit action-edit" style="cursor:pointer;color:blue;margin-right:10px"></i>
                <i class="fa fa-trash action-delete" style="cursor:pointer;color:red"></i>
                `
            }
        ]
    });
}

function addEventListener() {
    $("#btn-show-modal-add").on("click", function () {
        showModalAddNew();
    });

    $("#btn-show-modal-delete-all").on("click", function () {
        showModalDeleteAll();
    });

    $("#modal-editor #btn-add").on("click", function () {
        gSTUDENT_ENTITY.createNewStudent();
    });

    $("#modal-editor #btn-edit").on("click", function () {
        gSTUDENT_ENTITY.updateStudent();
    });

    $("#modal-editor #btn-delete").on("click", function () {
        gSTUDENT_ENTITY.deleteStudent();
    });

    $("#modal-editor #btn-delete-all").on("click", function () {
        gSTUDENT_ENTITY.deleteAllStudents();
    });

    $("#student-table").on("click", "tbody tr .action-edit", function () {
        var selectedRow = $(this).parents("tr");
        var selectedStudent = gDATA_TABLE.row(selectedRow).data();
        console.log(selectedStudent);
        gSTUDENT_ENTITY.student = selectedStudent;
        showModalEdit();
    });

    $("#student-table").on("click", "tbody tr .action-delete", function () {
        var selectedRow = $(this).parents("tr");
        var selectedStudent = gDATA_TABLE.row(selectedRow).data();
        //console.log(selectedStudent);
        gSTUDENT_ENTITY.student = selectedStudent;
        showModalDelete();
    });
}

function loadAllClassrooms() {
    $.ajax({
        url: BASE_URL + "/classrooms",
        method: "GET",
        success: function (data) {
            dataTableHelper.setDatasource(gDATA_TABLE, data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
}

function loadAllStudents() {
    $.ajax({
        url: BASE_URL + "/students",
        method: "GET",
        success: function (data) {
            dataTableHelper.setDatasource(gDATA_TABLE, data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
}

function loadClassroomToSelectClassroom() {
    $.ajax({
        url: BASE_URL + "/classrooms",
        method: "GET",
        success: function (data) {
            controlHelper.setSelectOptionDatasource($("#select-classroom"), data, "id", "classroomName")
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    });
}

function showModalAddNew() {
    $("#modal-editor .modal-title").text("Thêm mới");
    $("#modal-editor button").hide();
    $("#modal-editor #btn-close").show();
    $("#modal-editor #btn-add").show();
    $("#modal-editor .row").show();
    $("#modal-editor .confirm-delete").hide();
    modalHelper.emptyInputFields("#modal-editor");

    $("#modal-editor").modal();
}

function showModalEdit() {
    $("#modal-editor .modal-title").text("Chi tiết");
    $("#modal-editor button").hide();
    $("#modal-editor #btn-close").show();
    $("#modal-editor #btn-edit").show();
    $("#modal-editor .row").show();
    $("#modal-editor .confirm-delete").hide();
    gSTUDENT_ENTITY.fillDataToForm();

    $("#modal-editor").modal();
}

function showModalDelete() {
    $("#modal-editor .modal-title").text("Xóa");
    $("#modal-editor button").hide();
    $("#modal-editor #btn-close").show();
    $("#modal-editor #btn-delete").show();
    $("#modal-editor .row").hide();
    $("#modal-editor .confirm-delete").html(`<h6>Bạn có chắc chắn muốn xóa học sinh "${gSTUDENT_ENTITY.student.studentName}" không?</h6>`);
    $("#modal-editor .confirm-delete").show();

    $("#modal-editor").modal();
}

function showModalDeleteAll() {
    $("#modal-editor .modal-title").text("Xóa tất cả");
    $("#modal-editor button").hide();
    $("#modal-editor #btn-close").show();
    $("#modal-editor #btn-delete-all").show();
    $("#modal-editor .row").hide();
    $("#modal-editor .confirm-delete").html(`<h6>Bạn có chắc chắn muốn xóa tất cả các lớp không?</h6>`);
    $("#modal-editor .confirm-delete").show();

    $("#modal-editor").modal();
}

class Classroom {
    constructor(id, classroomCode, classroomName, teacherName, teacherPhoneNumber) {
        this.id = id;
        this.classroomCode = classroomCode;
        this.classroomName = classroomName;
        this.teacherName = teacherName;
        this.teacherPhoneNumber = teacherPhoneNumber;
    }
}

class Student {
    constructor(id, studentCode, studentName, gender, birthday, address, phoneNumber, classroomId) {
        this.id = id;
        this.studentCode = studentCode;
        this.studentName = studentName;
        this.gender = gender;
        this.birthday = birthday;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.classroomId = classroomId;
    }
}

class StudentEntity {
    constructor() {
        //this.classroomEntity = new ClassroomEntity();
        this.student = new Student();
        this.classroomId = null;
    }

    getData() {
        this.student.studentCode = $("#modal-editor #student-code").val().trim();
        this.student.studentName = $("#modal-editor #student-name").val().trim();
        this.student.gender = $("#modal-editor #select-gender").val().trim();

        let date = new Date($("#modal-editor #birthday").val());
        console.log("get data > date: ", date);
        let birthday = date.getTime();
        console.log("get data > birthday: ", birthday);
        this.student.birthday = birthday;
        this.student.address = $("#modal-editor #address").val().trim();
        this.student.phoneNumber = $("#modal-editor #phone").val().trim();

        let classroomId = $("#modal-editor #select-classroom").val().trim();
        this.classroomId = classroomId;
    }

    fillDataToForm() {
        $.ajax({
            url: BASE_URL + "/students/" + this.student.id,
            method: "GET",
            async: false,
            success: function (data) {
                this.student = data;

                $("#modal-editor #student-code").val(this.student.studentCode);
                $("#modal-editor #student-name").val(this.student.studentName);
                $("#modal-editor #select-gender").val(this.student.gender);
                let birthday = new Date(this.student.birthday);
                let formatedBirthday = dateTimeHelper.convertDateToHTML5String(birthday);
                console.log("birthday: ", birthday);
                console.log("formatedBirthday: ", formatedBirthday);
                $("#modal-editor #birthday").val(formatedBirthday);
                $("#modal-editor #address").val(this.student.address);
                $("#modal-editor #phone").val(this.student.phoneNumber);
                $("#modal-editor #select-classroom").val(this.student.classroomId);
            },
            error: function (jqXHR, statusText, errorThrown) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    createNewStudent() {
        this.getData();
        console.log(this.student);
        console.log(JSON.stringify(this.student));

        $.ajax({
            url: BASE_URL + "/students/create/" + this.classroomId,
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify(this.student),
            success: function (data) {
                alert("Tạo mới thành công");
                loadAllStudents();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    updateStudent() {
        this.getData();
        console.log(this.student);
        console.log("student id: ", this.student.id);
        console.log("calssroom id: ", this.classroomId);

        $.ajax({
            url: BASE_URL + "/students/update/" + this.student.id + "/" + this.classroomId,
            method: "PUT",
            contentType: "application/json",
            data: JSON.stringify(this.student),
            success: function (data) {
                alert("Cập nhật thành công");
                loadAllStudents();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    deleteStudent() {
        $.ajax({
            url: BASE_URL + "/students/delete/" + this.student.id,
            method: "DELETE",
            success: function (data) {
                alert("Xóa thành công");
                loadAllStudents();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR, statusText, errorThrown) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    deleteAllStudents() {
        $.ajax({
            url: BASE_URL + "/students/delete/",
            method: "DELETE",
            success: function (data) {
                alert("Xóa thành công");
                loadAllStudents();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR, statusText, errorThrown) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }



}

class ClassroomEntity {
    constructor() {
        this.classroom = new Classroom();
    }

    getData() {
        this.classroom.classroomCode = $("#modal-editor #classroom-code").val().trim();
        this.classroom.classroomName = $("#modal-editor #classroom-name").val().trim();
        this.classroom.teacherName = $("#modal-editor #teacher-name").val().trim();
        this.classroom.teacherPhoneNumber = $("#modal-editor #teacher-phone-number").val().trim();
    }

    fillDataToForm() {
        $.ajax({
            url: BASE_URL + "/classrooms/" + this.classroom.id,
            method: "GET",
            success: function (data) {
                this.classroom = data;
                $("#modal-editor #classroom-code").val(this.classroom.classroomCode);
                $("#modal-editor #classroom-name").val(this.classroom.classroomName);
                $("#modal-editor #teacher-name").val(this.classroom.teacherName);
                $("#modal-editor #teacher-phone-number").val(this.classroom.teacherPhoneNumber);
            },
            error: function (jqXHR, statusText, errorThrown) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    createNewClassroom() {
        this.getData();
        console.log(this.classroom);

        $.ajax({
            url: BASE_URL + "/classrooms/create",
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify(this.classroom),
            success: function (data) {
                alert("Tạo mới thành công");
                loadAllClassrooms();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    updateClassroom() {
        this.getData();
        $.ajax({
            url: BASE_URL + "/classrooms/update/" + this.classroom.id,
            method: "PUT",
            contentType: "application/json",
            data: JSON.stringify(this.classroom),
            success: function (data) {
                alert("Cập nhật thành công");
                loadAllClassrooms();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR, statusText, errorThrown) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    deleteClassroom() {
        $.ajax({
            url: BASE_URL + "/classrooms/delete/" + this.classroom.id,
            method: "DELETE",
            success: function (data) {
                alert("Xóa thành công");
                loadAllClassrooms();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR, statusText, errorThrown) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }

    deleteAllClassrooms() {
        $.ajax({
            url: BASE_URL + "/classrooms/delete",
            method: "DELETE",
            success: function (data) {
                alert("Xóa tất cả thành công");
                loadAllClassrooms();
                $("#modal-editor").modal("hide");
            },
            error: function (jqXHR, statusText, errorThrown) {
                errorHandler.showErrorMessage(jqXHR);
            }
        });
    }
}



