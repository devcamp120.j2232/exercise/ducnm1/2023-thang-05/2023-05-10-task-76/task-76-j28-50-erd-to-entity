var pageContentMap = new Map();
pageContentMap.set('1', "customerPage.html");
pageContentMap.set('2', "customerPage.html");
pageContentMap.set('3', "customerPage.html");

function loadPageContent(htmlFile, desination) {
    const xhr = new XMLHttpRequest();
    xhr.onload = function () {
        var htmlContent = xhr.responseText;

        var startTag = "<body>";
        var endTag = "</body>";

        var startIndex = htmlContent.indexOf(startTag) + startTag.length;
        var endIndex = htmlContent.indexOf(endTag);

        var bodyContent = htmlContent.substring(startIndex, endIndex);
        console.log(bodyContent);
    };

    xhr.open('GET', htmlFile);
    xhr.send();
}

function setActiveNavLink(element) {
    $(element).closest("ul").find(".nav-link").removeClass("active");
    $(element).find(".nav-link").addClass("active");
}

function addEventListeners() {
    $("[data-view]").on("click", function () {
        let dataViewIndex = $(this).attr("data-view");
        setActiveNavLink(this);
        loadPageContent(pageContentMap.get(dataViewIndex), $("#content-wrapper"));
    });
}

$(document).ready(function () {
    addEventListeners();
});