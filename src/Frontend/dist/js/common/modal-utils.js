export function setTitle(selector, title) {
    document.querySelector(selector).textContent = title;
}

export function emptyInputFields(modalSelector) {
    const modal = document.querySelector(modalSelector);
    const textInputs = modal.querySelectorAll('input[type="text"]');
    textInputs.forEach(input => {
        input.value = "";
    });
}