export function setSelectOptionDatasource(selectElement, datasource, value, text) {
    datasource.forEach(element => {
        $("<option>").val(element[value])
            .text(element[text])
            .appendTo(selectElement);
    });

}