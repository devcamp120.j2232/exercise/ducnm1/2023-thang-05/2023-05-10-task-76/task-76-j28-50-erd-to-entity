package com.devcamp.pizza365.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.repository.IPaymentRepository;

@Service
public class PaymentService {
    @Autowired
    private IPaymentRepository paymentRepository;

    public List<Payment> getAllPayments() {
        return paymentRepository.findAll();
    }

    public Payment getPaymentById(int id) {
        Optional<Payment> paymentOptional = paymentRepository.findById(id);
        return paymentOptional.orElseThrow(() -> new IllegalArgumentException("Payment not found!"));
    }

    public Payment createNewPayment(Payment paymentBodyRequest) {
        Payment newPayment = new Payment();
        newPayment.setAmmount(paymentBodyRequest.getAmmount());
        newPayment.setCheckNumber(paymentBodyRequest.getCheckNumber());
        newPayment.setCustomer(paymentBodyRequest.getCustomer());
        newPayment.setPaymentDate(paymentBodyRequest.getPaymentDate());

        paymentRepository.save(newPayment);
        return newPayment;
    }

    public Payment updatePaymentById(Integer id, Payment paymentBodyRequest) {
        Payment foundPayment = this.getPaymentById(id);

        foundPayment.setAmmount(paymentBodyRequest.getAmmount());
        foundPayment.setCheckNumber(paymentBodyRequest.getCheckNumber());
        foundPayment.setCustomer(paymentBodyRequest.getCustomer());
        foundPayment.setPaymentDate(paymentBodyRequest.getPaymentDate());

        paymentRepository.save(foundPayment);
        return foundPayment;
    }

    public void deletePaymentById(Integer id) {
        paymentRepository.deleteById(id);
    }

    public void deleteAllPayments() {
        paymentRepository.deleteAll();
    }

}
