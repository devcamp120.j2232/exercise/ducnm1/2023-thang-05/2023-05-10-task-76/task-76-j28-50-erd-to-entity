package com.devcamp.pizza365.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.IOrderRepository;

@Service
public class OrderService {
    @Autowired
    private IOrderRepository orderRepository;

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public Order getOrderById(int id) {
        Optional<Order> OrderOptional = orderRepository.findById(id);
        return OrderOptional.orElseThrow(() -> new IllegalArgumentException("Order not found!"));
    }

    public Order createNewOrder(Order OrderBodyRequest) {
        return orderRepository.save(OrderBodyRequest);
    }

    public Order updateOrderById(Integer id, Order OrderBodyRequest) {
        Order foundOrder = this.getOrderById(id);
        orderRepository.save(foundOrder);
        return foundOrder;
    }

    public void deleteOrderById(Integer id) {
        orderRepository.deleteById(id);
    }

    public void deleteAllOrders() {
        orderRepository.deleteAll();
    }
}
