package com.devcamp.pizza365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.entity.Payment;

public interface IPaymentRepository extends JpaRepository<Payment, Integer> {
    @Query(value = "SELECT * FROM payments p WHERE p.check_number like %:checkNumber%", nativeQuery = true)
    public List<Payment> findPaymentByCheckNumberLike(@Param("checkNumber") String checkNumber);

    public List<Payment> findByCheckNumberLike(String checkNumber);

}
